const express = require('express');
const morgan = require('morgan');
const getFile= require('./api/modules/getFile');
const createFile= require('./api/modules/createFile');
const getFiles= require('./api/modules/getFiles');
const PORT =8080;
const app = express();
app.use(express.json());
const logger = morgan('dev');
app.use(logger);

app.get('/api/files/:filename', getFile);
app.get('/api/files', getFiles);
app.post('/api/files', createFile);

app.listen(PORT, () => {
    console.log(`Server has been started on ${PORT}...`)
});
