# Node.js_HW1

Homework project, using NodeJS with implementation web-server which hosts and serves files.

## Getting started

There are 3 allowed requests:

1. POST request to path /api/files allowes You to create file. 

You must to pass filename(with extension) & content of file in body of Your request. 
Allowed extensions are  'log','txt','json','yaml','xml','js'.

2. GET request to path /api/files return list of existing files.

3. GET request to path /api/files/:filename return specific file  with content.

