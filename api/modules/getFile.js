const fs = require('fs');
const filesStoragePath = './api/files/';

const getFile = (req, res) => {
        const filesArr = [];
        fs.readdir(filesStoragePath, (error,files) => {
            try {
                files.forEach(file => {
                    filesArr.push(file);
                });
                if(filesArr.includes(req.params.filename)) {
                    fs.readFile(`${filesStoragePath}`+`${req.params.filename}`, (error, data) => {
                        if (error) {
                            res.statusMessage = 'Internal server error';
                            res.status(500).send({ 'message': 'Server error' })
                        }
                        fs.stat(`${filesStoragePath}`+`${req.params.filename}`, (err, stats) => {
                            if (err) {
                                res.statusMessage = 'Internal server error';
                                res.status(500).send({ 'message': 'Server error' })
                            }
                            res.statusMessage = 'Success';
                            res.status(200).send({message: 'Success', filename: req.params.filename,
                                                    content: data.toString(), extension: req.params.filename.split('.').pop(),
                                                    uploadedDate: stats.mtime});
                        });
                    });
                }
                else {throw error}
            } catch(error) {
                res.statusMessage = 'Bad request';
                res.status(400).send({message: `No file with '${req.params.filename}' filename found`});
            }
        });
}
module.exports = getFile;