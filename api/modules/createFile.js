const fs = require('fs');

if (!fs.existsSync('./api/files/')) {
    fs.mkdir('./api/files/', err => {
        if (err) throw err; // не удалось создать папку
        console.log('Папка успешно создана');
    });
}
const filesStoragePath = './api/files/';
const createFile = (req, res) => {

    const { filename, content } = req.body;
    if (req.body.filename === null) {
        console.log('filename empty');
        console.log(filename);

        res.statusMessage = "Bad request";
        res.status(400).send({ message: "Please specify 'filename' parameter" });

    }
    const allowedExtensions = ['log', 'txt', 'json', 'yaml', 'xml', 'js'];

    if (filename === undefined) {
        res.statusMessage = "Bad request";
        res.status(400).send({ message: "Please specify 'filename' parameter" });
    }

    const fileExtension = filename.split('.').pop();
    if (fs.existsSync(filesStoragePath)) {
        fs.readdir(filesStoragePath, (error, files) => {
            const currentFile = files.filter(file => file === filename);
            if (currentFile.length > 0) {
                res.status(400).send({ "message": "File is already exist" })
            } else if (!filename) {
                res.statusMessage = "Bad request";
                res.status(400).send({ message: "Please specify 'filename' parameter" });
            } else if (!content) {
                res.statusMessage = "Bad request";
                res.status(400).send({ message: "Please specify 'content' parameter" });
            } else if (!allowedExtensions.includes(fileExtension)) {
                res.statusMessage = "Bad request";
                res.status(400).send({ message: "This file extension is not supported" });
            } else {
                res.statusMessage = "Success";
                res.status(200).send({ message: 'File created successfully' });
                fs.writeFile(`${filesStoragePath}` + `${filename}`, content, (err) => {});
            }
        });
    } else {
        res.statusMessage = "Internal server error";
        res.status(500).send({ message: "Server error" });
    }
}

module.exports = createFile;