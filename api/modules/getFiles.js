const fs = require('fs');
const filesStoragePath = './api/files/';

const getFiles = (req, res) => {
    const filesArr = [];
    if (fs.existsSync(filesStoragePath)) {
        try {
            fs.readdir(filesStoragePath, (error, files) => {
                files.forEach(file => {
                    filesArr.push(file);
                });
                res.statusMessage = "Success";
                res.status(200).send({ message: 'Success', files: filesArr });
            });
        } catch (error) {
            res.statusMessage = "Bad request";
            res.status(400).send({ message: 'Client error' });
        }
    } else {
        res.statusMessage = "Internal server error";
        res.status(500).send({ message: "Server error" });
    }
}

module.exports = getFiles;